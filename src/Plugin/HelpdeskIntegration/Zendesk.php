<?php

namespace Drupal\helpdesk_zendesk\Plugin\HelpdeskIntegration;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\comment\CommentInterface;
use Drupal\helpdesk_integration\Entity\Issue;
use Drupal\helpdesk_integration\HelpdeskInterface;
use Drupal\helpdesk_integration\HelpdeskPluginException;
use Drupal\helpdesk_integration\IssueInterface;
use Drupal\helpdesk_integration\PluginBase;
use Drupal\helpdesk_zendesk\Service;
use Drupal\migrate\Audit\AuditException;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zendesk\API\Exceptions\ApiResponseException;
use Zendesk\API\Exceptions\AuthException;
use Zendesk\API\Exceptions\MissingParametersException;
use Zendesk\API\HttpClient;

/**
 * Plugin implementation of the Zendesk helpdesk.
 *
 * @HelpdeskPlugin(
 *   id = "zendesk",
 *   label = @Translation("Zendesk"),
 *   description = @Translation("Provides the Zendesk helpdesk plugin.")
 * )
 */
class Zendesk extends PluginBase {

  use StringTranslationTrait;

  /**
   * The Zendesk service.
   *
   * @var \Drupal\helpdesk_zendesk\Service
   */
  protected Service $zendeskService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Zendesk {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->zendeskService = $container->get('helpdesk_zendesk.service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function settingKeys(): array {
    return [
      'subdomain',
      'admin_user',
      'api_token',
      'group',
      'states',
      'state_closed',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(HelpdeskInterface $helpdesk, array $required): array {
    $form['subdomain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subdomain'),
      '#default_value' => $helpdesk->get('subdomain'),
    ] + $required;
    $form['admin_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Admin User'),
      '#default_value' => $helpdesk->get('admin_user'),
    ] + $required;
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#default_value' => $helpdesk->get('api_token'),
    ] + $required;
    $states = $helpdesk->get('subdomain') ? $this->getStates($helpdesk) : [];
    $form['states'] = [
      '#type' => 'value',
      '#value' => $states,
    ];
    $form['state_closed'] = [
      '#type' => 'select',
      '#title' => $this->t('Closed ticket state'),
      '#default_value' => $helpdesk->get('state_closed'),
      '#options' => $states,
    ];

    return $form;
  }

  /**
   * Get all states from the helpdesk.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk entity with the instance's configuration.
   *
   * @return array
   *   List of all active states indexed by their remote state ID.
   */
  private function getStates(HelpdeskInterface $helpdesk): array {
    $states = [];
    try {
      $client = $this->zendeskService->getClient($helpdesk);
      $states = $this->zendeskService->getStates($client);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return $states;
  }

  /**
   * {@inheritdoc}
   */
  public function createIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void {
    try {
      $client = $this->zendeskService->getClient($helpdesk, $issue->getOwner()->getEmail());
      $uploadTokens = $this->zendeskService->uploadTokens($client, $issue->get('attachments')
        ->getValue());
      $bodyValue = $issue->get('body')->value;
      if (!$bodyValue) {
        $bodyValue = 'no text';
      }
      $remote_issue = $this->zendeskService->createIssue($client, $issue->label(), $bodyValue, $uploadTokens);
      $issue->set('extid', $remote_issue->request->id);

    }
    catch (\Exception $e) {
      throw new HelpdeskPluginException($this->getErrorMessage($e));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addCommentToIssue(HelpdeskInterface $helpdesk, IssueInterface $issue, CommentInterface $comment): void {
    try {
      $client = $this->zendeskService->getClient($helpdesk, $issue->getOwner()->getEmail());
      $ticket_id = $issue->get('extid')->value;
      $uploadTokens = $this->zendeskService->uploadTokens($client, $comment->get('field_attachments')
        ->getValue());
      $client->requests()->update($ticket_id, [
        'comment' => [
          'html_body' => $comment->get('comment_body')->value,
          'uploads' => $uploadTokens,
        ],
      ]);

      $remote_comments = $client->requests($ticket_id)
        ->comments()
        ->findAll()->comments;
      foreach ($remote_comments as $remote_comment) {
        $comment->set('field_extid', $remote_comment->id);
      }
    }
    catch (ApiResponseException | AuthException  $e) {
      throw new HelpdeskPluginException($e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function resolveIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void {
    $closed_state = $helpdesk->get('state_closed');
    $client = $this->zendeskService->getClient($helpdesk);

    try {
      $client->tickets()->update($issue->get('extid')->value, [
        'status' => $closed_state,
      ]);
    }
    catch (\Exception $exception) {
      $this->getErrorMessage($exception);
    }

    $states = $helpdesk->get('states');
    $issue->set('status', $states[$closed_state] ?? '');
  }

  /**
   * {@inheritdoc}
   */
  public function getAllIssues(HelpdeskInterface $helpdesk, UserInterface $user, int $since = 0): array {
    $states = $helpdesk->get('states');
    $state_closed = (string) $helpdesk->get('state_closed');
    $client = $this->zendeskService->getClient($helpdesk);
    $issues = [];
    try {
      // NOTE: We do the update date check programmatically, because Zendesk
      // needs a few  minutes to update the search indexes.
      // See https://support.zendesk.com/hc/en-us/articles/203663226
      $requests = $client
        ->users($this->getUserData($user, $helpdesk, self::REMOTE_ID))
        ->requests()
        ->findAll()->requests;
      foreach ($requests as $request) {
        /** @var \Drupal\helpdesk_integration\IssueInterface $issue */
        $issue = Issue::create([
          'helpdesk' => $helpdesk->id(),
          'extid' => $request->id,
          'resolved' => (string) $request->status === $state_closed,
          'title' => $request->subject,
          'status' => $states[$request->status] ?? '',
          'body' => [
            'value' => empty($request->description) ? '' : $request->description,
            'format' => 'basic_html',
          ],
          'created' => strtotime($request->created_at),
          'changed' => strtotime($request->updated_at),
        ]);

        $this->handleComments($client, $request, $issue, $user);
        $issues[] = $issue;
      }
    }
    catch (ApiResponseException | AuditException | AuthException $e) {
      throw new HelpdeskPluginException($e);
    }
    return $issues;
  }

  /**
   * Handle ticket comments.
   *
   * The first comment is special and must be handled
   * differently. The attachments, if present, are stored in the issue itself.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The HTTP client.
   * @param object $ticket
   *   The ticket.
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue.
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @throws \Zendesk\API\Exceptions\ApiResponseException
   * @throws \Zendesk\API\Exceptions\AuthException
   */
  private function handleComments(HttpClient $client, object $ticket, IssueInterface $issue, UserInterface $user): void {
    $count = 0;
    $comments = $client->requests($ticket->id)->comments()->findAll()->comments;
    foreach ($comments as $comment) {
      if ($count > 0) {
        $comment_id = $issue->addComment(
          $comment->id,
          $comment->body,
          $user,
          strtotime($comment->created_at),
          strtotime($comment->created_at)
        );
        $this->handleCommentAttachments($comment, $issue, $comment_id);
      }
      else {
        foreach ($comment->attachments as $attachment) {
          $issue->addIssueAttachment($attachment->file_name, $attachment->content_url);
        }
      }
      $count++;
    }
  }

  /**
   * Handles the attachments of the ticket comments.
   *
   * @param object $comment
   *   The comment.
   * @param \Drupal\helpdesk_integration\IssueInterface $issue
   *   The issue.
   * @param int $comment_id
   *   The comment ID.
   */
  private function handleCommentAttachments(object $comment, IssueInterface $issue, int $comment_id): void {
    foreach ($comment->attachments as $attachment) {
      $issue->addCommentAttachment(
        $comment_id,
        $attachment->file_name,
        $attachment->content_url
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function pushUser(HelpdeskInterface $helpdesk, UserInterface $user): string {
    $client = $this->zendeskService->getClient($helpdesk);
    try {
      if ($remote_id = $this->getUserData($user, $helpdesk, self::REMOTE_ID)) {
        $remoteUser = $client->users()->find($remote_id);
        if ($remoteUser->user->active) {
          $this->zendeskService->updateUser($client, $remoteUser->user, $user);
          return $remote_id;
        }
        else {
          $userFromPlatform = $this->zendeskService->createUser($client, $user);
          return $userFromPlatform->user->id;
        }
      }
      else {
        $remote_users = $client->users()
          ->search(['query' => $user->getEmail()]);
        if (!empty($remote_users->users)) {
          $this->zendeskService->updateUser($client, $remote_users->users[0], $user);
          return $remote_users->users[0]->id;
        }
        else {
          $userFromPlatform = $this->zendeskService->createUser($client, $user);
          return $userFromPlatform->user->id;
        }
      }
    }
    catch (\Exception $e) {
      throw new HelpdeskPluginException($this->getErrorMessage($e));
    }

  }

  /**
   * {@inheritDoc}
   */
  public function isUserLocked(HelpdeskInterface $helpdesk, UserInterface $user): bool {
    $client = $this->zendeskService->getClient($helpdesk);
    try {
      if ($remote_id = $this->getUserData($user, $helpdesk, self::REMOTE_ID)) {
        $remoteUser = $client->users()->find($remote_id);
        if ($remoteUser->user->suspended) {
          return TRUE;
        }
      }
    }
    catch (MissingParametersException $e) {
      throw new HelpdeskPluginException($e);
    }
    return FALSE;
  }

  /**
   * Handles the exception part for different exception codes.
   *
   * @param \Exception $exception
   *   The helpdesk exception.
   *
   * @return string
   *   The error message to throw.
   */
  private function getErrorMessage(\Exception $exception): string {
    $errorMessage = $exception->getMessage();
    if ($exception instanceof ApiResponseException && $exception->getCode() === 422) {
      $errorDetails = $exception->getErrorDetails();
      $errorMessage = '';
      foreach ($errorDetails as $detail) {
        $errorMessage .= $detail[0]->description . PHP_EOL;
      }
    }
    return $errorMessage;
  }

}
