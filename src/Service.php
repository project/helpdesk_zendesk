<?php

namespace Drupal\helpdesk_zendesk;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\helpdesk_integration\HelpdeskInterface;
use Drupal\helpdesk_integration\HelpdeskPluginException;
use Drupal\helpdesk_integration\Service as BaseService;
use Drupal\user\UserInterface;
use Zendesk\API\Exceptions\ApiResponseException;
use Zendesk\API\Exceptions\AuthException;
use Zendesk\API\Exceptions\CustomException;
use Zendesk\API\Exceptions\MissingParametersException;
use Zendesk\API\HttpClient;

/**
 * Service class for Zendesk communication.
 */
class Service {

  /**
   * The ticket restriction.
   */
  const TICKET_RESTRICTION = 'organization';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The helpdesk integration services.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected BaseService $service;

  /**
   * The created clients by username (email).
   *
   * @var array
   */
  private array $httpClients = [];

  /**
   * Service constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk integration services.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, BaseService $service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->service = $service;
  }

  /**
   * Gets the API Client.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The Helpdesk.
   * @param string|null $email
   *   The email.
   *
   * @return \Zendesk\API\HttpClient
   *   The HTTP Client.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function getClient(HelpdeskInterface $helpdesk, ?string $email = NULL): HttpClient {
    $userName = $email ?? $helpdesk->get('admin_user');
    if (array_key_exists($userName, $this->httpClients)) {
      return $this->httpClients[$userName];
    }
    $client = $this->doCreateClient($helpdesk, $userName);
    $this->httpClients[$userName] = $client;
    return $client;
  }

  /**
   * Creates the http client.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param string $userName
   *   The user name.
   *
   * @return \Zendesk\API\HttpClient
   *   The HTTP CLient.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function doCreateClient(HelpdeskInterface $helpdesk, string $userName): HttpClient {
    $subdomain = $helpdesk->get('subdomain');
    $token = $helpdesk->get('api_token');
    $client = new HttpClient($subdomain);
    try {
      $client->setAuth('basic', ['username' => $userName, 'token' => $token]);
    }
    catch (AuthException $e) {
      throw new HelpdeskPluginException($e);
    }
    return $client;
  }

  /**
   * Create the user.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The Http client.
   * @param \Drupal\user\UserInterface $user
   *   The Drupal user.
   *
   * @return object|null
   *   The user object.
   */
  public function createUser(HttpClient $client, UserInterface $user): ?object {
    return $client->users()->create($this->getUserArray($user));
  }

  /**
   * Updates the user.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The HTTP client.
   * @param object $remoteUser
   *   The remote user.
   * @param \Drupal\user\UserInterface $user
   *   The Drupal user.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function updateUser(HttpClient $client, object $remoteUser, UserInterface $user): void {
    $hasChanged = FALSE;
    if (strcasecmp($remoteUser->email, $user->getEmail()) !== 0) {
      $hasChanged = TRUE;
    }
    if ($remoteUser->name != $user->getDisplayName()) {
      $hasChanged = TRUE;
    }
    if ($hasChanged) {
      $client->users()->update($remoteUser->id, $this->getUserArray($user));
      $this->deleteAllOldEmailIdentities($client, $remoteUser->id, $user->getEmail());
    }
  }

  /**
   * Gets the values of the user as array.
   *
   * @param \Drupal\user\UserInterface $user
   *   The Drupal user.
   *
   * @return array
   *   The users.
   */
  private function getUserArray(UserInterface $user): array {
    return [
      'name' => $user->getDisplayName(),
      'email' => $user->getEmail(),
      'verified' => TRUE,
      'ticket_restriction' => self::TICKET_RESTRICTION,
    ];
  }

  /**
   * Gets the phone number identities of a Zendesk user.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The HTTP client.
   * @param string $remoteUserId
   *   The remote user.
   *
   * @return array
   *   The phone identities.
   *
   * @throws \Zendesk\API\Exceptions\ApiResponseException
   * @throws \Zendesk\API\Exceptions\AuthException
   */
  public function getUserPhoneIdentities(HttpClient $client, string $remoteUserId): array {
    $identities = [];
    foreach ($client->users($remoteUserId)
      ->identities()
      ->findAll()->identities as $identity) {
      if ($identity->type == 'phone_number') {
        $identities[] = $identity;
      }
    }

    return $identities;
  }

  /**
   * Creates a phone number identity for the given user.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The http client.
   * @param string $remoteUserId
   *   The remote user id.
   * @param string $phoneNumber
   *   The phone number.
   */
  public function createPhoneNumber(HttpClient $client, string $remoteUserId, string $phoneNumber): void {
    $client->users($remoteUserId)->identities()->create([
      'type' => 'phone_number',
      'value' => $phoneNumber,
    ]);
  }

  /**
   * Sets a given phone number to shared.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The http client.
   * @param string $remoteUserId
   *   The remote user id.
   * @param string $phoneNumber
   *   The phone number.
   */
  public function setSharedPhoneNumber(HttpClient $client, string $remoteUserId, string $phoneNumber): void {
    $client->users()->update((int) $remoteUserId, [
      'shared_phone_number' => TRUE,
      'phone' => $phoneNumber,
    ]);
  }

  /**
   * Delete all old email identities, but not the current one.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The http client.
   * @param string $remoteUserId
   *   The remote user id.
   * @param string $currentEmail
   *   The current email.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function deleteAllOldEmailIdentities(HttpClient $client, string $remoteUserId, string $currentEmail): void {
    try {
      foreach ($this->getUserIdentities($client, $remoteUserId, 'email') as $identity) {
        if (strcasecmp($currentEmail, $identity->value) === 0) {
          continue;
        }
        $client->users($remoteUserId)->identities()->delete($identity->id);
      }
    }
    catch (ApiResponseException | AuthException | MissingParametersException $e) {
      throw new HelpdeskPluginException($e);
    }
  }

  /**
   * Gets the user identities by type.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The http client.
   * @param string $remoteUserId
   *   The remote user id.
   * @param string $identityType
   *   The identity type.
   *
   * @return array
   *   The user identities.
   *
   * @throws \Zendesk\API\Exceptions\ApiResponseException
   * @throws \Zendesk\API\Exceptions\AuthException
   */
  public function getUserIdentities(HttpClient $client, string $remoteUserId, string $identityType): array {
    $identities = [];
    foreach ($client->users($remoteUserId)->identities()->findAll()
      ->identities as $identity) {
      if ($identity->type == $identityType) {
        $identities[] = $identity;
      }
    }

    return $identities;
  }

  /**
   * Creates an issue.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The Http client.
   * @param string $subject
   *   The subject.
   * @param string $body
   *   The body.
   * @param array $uploadTokens
   *   The upload tokens.
   *
   * @return object
   *   The issue.
   */
  public function createIssue(HttpClient $client, string $subject, string $body, array $uploadTokens): object {
    return $client->requests()->create([
      'subject' => $subject,
      'comment' => [
        'html_body' => $body,
        'uploads' => $uploadTokens,
      ],
    ]);
  }

  /**
   * Gets the state ticket fields.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function getStates(HttpClient $client): array {
    $states = [];
    try {
      foreach ($client->ticketFields()->findAll()->ticket_fields as $state) {
        if ($state->type === 'status') {
          foreach ($state->system_field_options as $option) {
            $states[$option->value] = $option->name;
          }
        }
      }
    }
    catch (ApiResponseException | AuthException $e) {
      throw new HelpdeskPluginException($e);
    }
    return $states;
  }

  /**
   * Uploads the given attachments and returns the tokens.
   *
   * @param \Zendesk\API\HttpClient $client
   *   The Http client.
   * @param array $attachments
   *   The attachments.
   *
   * @return array
   *   The upload tokens.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  public function uploadTokens(HttpClient $client, array $attachments): array {
    $uploadTokens = [];
    /** @var \Drupal\file\Plugin\Field\FieldType\FileItem $attachment */
    foreach ($attachments as $attachment) {
      try {
        /** @var \Drupal\file\FileInterface $file */
        $file = $this->entityTypeManager->getStorage('file')
          ->load($attachment['target_id']);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
        throw new HelpdeskPluginException($e);
      }
      $attachmentData = [
        'file' => $file->getFileUri(),
        'type' => $file->getMimeType(),
        'name' => $file->getFilename(),
      ];
      try {
        $uploadTokens[] = $client->attachments()
          ->upload($attachmentData)->upload->token;
      }
      catch (CustomException | MissingParametersException | \Exception $e) {
        throw new HelpdeskPluginException($e);
      }
    }
    return $uploadTokens;
  }

}
