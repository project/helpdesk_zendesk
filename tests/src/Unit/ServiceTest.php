<?php

namespace Drupal\Tests\helpdesk_zendesk;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\helpdesk_integration\HelpdeskInterface;
use Drupal\helpdesk_integration\Service as BaseService;
use Drupal\helpdesk_zendesk\Service;
use Zendesk\API\Utilities\Auth;

/**
 * Tests the service class.
 *
 * @group helpdesk_zendesk
 */
class ServiceTest extends UnitTestCase {

  /**
   * Test getting a http client for an administrator.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   * @throws \Zendesk\API\Exceptions\AuthException
   */
  public function testGetClientForAdmin(): void {
    $service = new Service($this->createMock(EntityTypeManagerInterface::class),
      $this->createMock(BaseService::class));
    $helpDesk = $this->createMock(HelpdeskInterface::class);
    $helpDesk->expects($this->exactly(3))->method('get')
      ->willReturn('max@example.com', 'sub', 'token');
    $client = $service->getClient($helpDesk);
    $expectedAuth = new Auth('basic', [
      'username' => 'max@example.com',
      'token' => 'token',
    ]);
    $this->assertEquals($expectedAuth, $client->getAuth());
    $this->assertEquals('https://sub.zendesk.com:443/', $client->getApiUrl());
  }

  /**
   * Test getting a http client for a customer.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   * @throws \Zendesk\API\Exceptions\AuthException
   */
  public function testGetClientForCustomer(): void {
    $service = new Service($this->createMock(EntityTypeManagerInterface::class),
      $this->createMock(BaseService::class));
    $helpDesk = $this->createMock(HelpdeskInterface::class);
    $helpDesk->expects($this->exactly(2))->method('get')
      ->willReturn('sub', 'token');
    $client = $service->getClient($helpDesk, 'customer@example.com');
    $expectedAuth = new Auth('basic', [
      'username' => 'customer@example.com',
      'token' => 'token',
    ]);
    $this->assertEquals($expectedAuth, $client->getAuth());
    $this->assertEquals('https://sub.zendesk.com:443/', $client->getApiUrl());
  }

}
